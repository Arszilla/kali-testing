# Kali i3 | Custom ISO

[![CI/CD][CI/CD]][CI/CD URL]
[![LICENSE][LICENSE]][LICENSE URL]
[![Twitter][Twitter]][Twitter URL]
[![Github Sponsors][Github Sponsors]][Github Sponsors URL]
[![Ko-Fi][Ko-Fi]][Ko-Fi URL]

## Table of Contents
- [Disclaimer](#disclaimer)
- [Introduction](#introduction)
- [Pictures](#pictures)
- [Installation](#installation)
  - [Downloading](#downloading)
  - [Building](#building)
    - [Kali Debian Based Systems](#kali-debian-based-systems)
    - [Non-Kali Debian Based Systems](#non-kali-debian-based-systems)
- [Known Issues](#known-issues)
- [Bug Reports](#bug-reports)
- [Keybinds](#keybinds)
- [Customization](#customization)
- [FAQ](#faq)
- [Donations](#donations)
- [Acknowledgements](#acknowledgements)

## Disclaimer
As of 2023-01-02, `i3` and `i3-gaps` have officially merged with the release 
of `v4.22` of `i3`. As a result, all references to `i3-gaps` have been 
replaced/updated to `i3` in this project. Regardless of the change in core 
package(s), the purpose of this project is still the same and I will 
continue to maintain Kali i3 for the forseeable future.

It goes without saying that the repository will be renamed to `kali-i3`, 
instead of `kali_i3-gaps`.

## Introduction
This is my take on Kali, using i3 with gaps (i.e. i3-gaps back when it was a 
seperate thing) as a WM. I've done this because I loved how i3 worked on my 
new (well, it was new back in May 2019) laptop. It's really lightweight 
(compared to other WM's and DE's) while being reallycustomizable and easy to 
use.

## Pictures

![1][1]

![2][2]

![3][3]

![4][4]

## Installation
### Downloading
As of 2022-08, Kali i3 has a Gitlab CI/CD pipeline, allowing me to share 
ready-for-use images of Kali i3 to everyone. You can download Kali i3 and its 
build-log from the links below:
- [Kali i3 ISO][Kali i3 ISO]
- [Kali i3 Build Log][Kali i3 Build Log]

However, do constrains, the only version of the available image is `amd64`. 
Thus,  if you require `i386`/`arm64`/`armel`/`armhf` versions of Kali i3-gaps, 
please refer to [building](#building).

### Building
#### Kali Debian Based Systems
To build the ISO:

```
$ sudo apt install -y curl git live-build cdebootstrap
$ git clone https://gitlab.com/Arszilla/kali_i3.git
$ cd kali-i3
$ ./build.sh --variant i3 --verbose
```

By default, the command above will build a Kali i3 ISO based on your host's 
architecture. If you want another architecture (`i386`/`arm64`/`armel`/`armhf`), 
use the `--arch` flag:

```
$ ./build.sh --arch <i386/amd64/arm64/armel/armhf> --variant i3 --verbose
```

#### Non-Kali Debian Based Systems
If you are on a non-Kali Debian based system, check out Kali's
[guide][Kali Docs] on building the ISO.

---

**Once the script is finished, your image should be in `~/kali-i3/images`.**

## Known Issues
- In VMs, (tested on `VMware`) `backend = "glx";` (Line 119 in `picom.conf`) causes 
the VMs to act erratically (lag, keyboard to not function properly, etc.). To fix 
this, enable 3D acceleration for the VM in your virtualization software.
- In Live System instances there is no wallpaper present.
- Once the ISO is built and ran/installed, there won't be user directories (in 
`$HOME` or in `root`). This is because `xdg-user-dirs-update` doesn't run (during 
build, live-system or install phase). A manual fix for this is to run 
`xdg-user-dirs-update` in a terminal once the you log into the system.

## Bug Reports
I've been working on this since late 2019-11. There might be some errors here
and there; bugs and/or features from official Kali I've missed. If that's the
case and you find a bug and/or a missing feature:

- Fork the repository
- Make changes
- Submit a PR

I'll evaluate the changes ASAP and get back to you.

## Keybinds
Please refer to [$HOME/.config/i3/config.d/keybinds.conf][i3 keybinds] and 
[$HOME/.config/dunst/dunstrc][dunst keybinds] for a list of the keybinds used 
in Kali i3.

## Customization
All of the default customizations I've created for Kali i3 with the aide of 
Kali Team (to publish these customizations) can be found at the 
[i3-dotfiles][i3-dotfiles] package, available in Kali.

## FAQ
- To connect to a WiFi, you run `nmtui` in the terminal. Alternatively, click 
on the `Offline` tag for the WiFi on your polybar to get a `rofi` `dmenu` for 
a list of available WiFi APs.
- To manage your current display or multiple monitors, run `arandr` in `rofi`. 
Alternatively, you can use `xrandr` the terminal.

## Donations
If you like Kali i3 and would like to support me, please consider donating 
using the links below. Any amount is more than welcome as I will be using the 
funds for further projects, such as building a homelab to run `gitlab-runner` 
in my own environment, instead of on DigitalOcean.
- [Github Sponsors][Github Sponsors URL]
- [Ko-Fi][Ko-Fi URL]
- BTC:  `bc1qfp2a7pncxvq3s9qgtj0fp7k6v5rzy8g763u7uk`
- BCH:  `qz3s06xm9j6cj26qavstykwysf3xs92l3ymjpvut88`
- ETH:  `0x3FB9505DA434Ce308880261acbe56A4e321DdEFC`
- XLM:  `GD2ARSM32ZLYXWTL6WMRVCDNAVRRCDBG7GRHNEVGYF6UVOWSUL4GJKWO`
- DOGE: `DNPBgj2JVgYm17h8ybxkpYmC2LZmL91pUs`

## Acknowledgements
- [TJNull][TJNull] for encouraging me to do this, as well
as helping me out and being an amazing friend.
- The [Kali Team][Kali Team] for their work on 
[live-build-config][live-build-config], making this ISO and project possible.

[CI/CD]:                    https://img.shields.io/gitlab/pipeline-status/arszilla/kali-i3?branch=main&style=flat-square
[CI/CD URL]:                https://gitlab.com/arszilla/kali-i3/-/commits/main
[LICENSE]:                  https://img.shields.io/gitlab/license/arszilla/kali-i3?style=flat-square
[LICENSE URL]:              ./LICENSE
[Twitter]:                  https://img.shields.io/badge/Twitter-%231DA1F2.svg?style=flat-square&logo=Twitter&logoColor=white
[Twitter URL]:              https://twitter.com/arszilla
[Github Sponsors]:          https://img.shields.io/badge/sponsor-30363D?style=flat-square&logo=GitHub-Sponsors&logoColor=#EA4AAA
[Github Sponsors URL]:      https://github.com/sponsors/arszilla
[Ko-Fi]:                    https://img.shields.io/badge/Ko--fi-F16061?style=flat-square&logo=ko-fi&logoColor=white
[Ko-Fi URL]:                https://ko-fi.com/arszilla
[1]:                        /Pictures/1.png
[2]:                        /Pictures/2.png
[3]:                        /Pictures/3.png
[4]:                        /Pictures/4.png
[Kali i3 ISO]:              https://cdn.arszilla.download/kali/kali-i3-amd64.iso
[Kali i3 Build Log]:        https://cdn.arszilla.download/kali/kali-i3-amd64.log
[Kali Docs]:                https://www.kali.org/docs/development/live-build-a-custom-kali-iso/
[i3 keybinds]:              https://gitlab.com/kalilinux/packages/i3-dotfiles/-/blob/kali/master/dotfiles/etc/skel/.config/i3/config.d/keybinds.conf
[dunst keybinds]:           https://gitlab.com/kalilinux/packages/i3-dotfiles/-/blob/kali/master/dotfiles/etc/skel/.config/dunst/dunstrc
[i3-dotfiles]:              https://gitlab.com/kalilinux/packages/i3-dotfiles/
[i3wm Docs]:                https://i3wm.org/docs/userguide.html
[Debian Docs]:              https://wiki.debian.org/DefaultWebBrowser
[TJNull]:                   https://twitter.com/TJ_Null
[Kali Team]:                https://www.kali.org/about-us/
[live-build-config]:        https://gitlab.com/kalilinux/build-scripts/live-build-config
